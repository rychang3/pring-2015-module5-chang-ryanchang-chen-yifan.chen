
<?php
session_start();
require 'databasemodule5.php';
$u=$_SESSION["userid"];
$stmt = $mysqli->prepare("SELECT id, name, year, month, day, time FROM events WHERE userid=? ORDER BY events.year, events.month, events.day DESC");
$stmt->bind_param('i', $u);
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();
$stmt->bind_result($event_id, $event_name, $event_year, $event_month, $event_day, $event_time);
while($stmt->fetch()){
	echo "<h2>$event_name</h2>";
	echo "Date: $event_month $event_day, $event_year<br />";
	echo "Time: $event_time";
	echo "<button data-id='$event_id' name='delete'>Delete</button>" ;

}
?>