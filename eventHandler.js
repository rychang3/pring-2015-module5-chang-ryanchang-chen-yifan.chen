

function prepareEventAdd(dateobj, day)
{

	var y = dateobj.getFullYear();
	var m = dateobj.getMonth();
	var d = day;

	var xmlHttp = new XMLHttpRequest();
	var ni = document.getElementById("name");
	var n = ni.value;
	var ti = document.getElementById("time");
	var t = ti.value;
	xmlHttp.open("POST", "postevent.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", toggleForm, false);
	xmlHttp.send("name=" + n + "&time=" + t + "&year=" + y + "&month=" + m + "&day=" + d);	//send u and p to validate.php

}

function prepareEventRemove(id)
{

	var uid = id;
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "deleteevent.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.send("&uid=" + id);	//send u and p to validate.php

}
